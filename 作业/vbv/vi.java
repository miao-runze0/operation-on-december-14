package vbv;
import java.util.Scanner;
/*家贸易公司有4位销售员，每位销售员负责销售相同的4种商品
使用二维数组，编写一个程序，接收每名销售员销售的各种产品的数量。
 打印产品销售明细表，
明细表包括：每类产品的销售总数，
以及每位销售员销售的产品数量占总销售的百分比。
使用以下公式：
销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）?100
总销售量指各类产品销售量的总和（）
商品	销售1	销售2	销售3	销售4	销售总数
商品1					
商品2					
商品3					
商品4					*/
public class vi {
	public static void main(String[] args) {
		Scanner sr =new Scanner(System.in);
		int[][] st=new int[4][4];
		int su=0;
		for (int i = 0; i < st.length; i++) {
			System.out.println("请输入第"+(i+1)+"个人的第1件商品");
			st[i][0]=sr.nextInt();
			System.out.println("请输入第"+(i+1)+"个人的第2件商品");
			st[i][1]=sr.nextInt();
			System.out.println("请输入第"+(i+1)+"个人的第3件商品");
			st[i][2]=sr.nextInt();
			System.out.println("请输入第"+(i+1)+"个人的第4件商品");
			st[i][3]=sr.nextInt();
		}
		System.out.println("=========销售成绩单==========");
		System.out.println("商品\t销售1\t销售2\t销售3\t销售4\t销售总数\t百分比");
		for (int i = 0; i < 4; i++) {
			su=st[i][0]+st[i][1]+st[i][2]+st[i][3];
			double bfb=st[i][3];
			System.out.print("商品"+(i+1)+"\t");
			System.out.print(st[i][0]+"\t"+st[i][1]+"\t"+st[i][2]+"\t"+st[i][3]+"\t");
			System.out.print(su);
			System.out.println("\t"+(bfb/su)*100+"%");
			
		
	}
	}

	}


